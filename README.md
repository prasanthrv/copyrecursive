﻿# CopyRecursive

This is a simple utility program to copy files recursively from a set of source directories to a destination directory.  
Language: C#


## Features:
  - Copies files recursively from multiple source directories
  - Can move or copy files
  - Eliminates duplicates
  - Files with same name are copied with pattern [File_1, File_2 ...]

## Usage:

```
  CopyRecursive -s <src_dir1> <src_dir2> -d <dest_dir> -f "txt;jpg" -m0

  -s, --src       Required. Source directories to read from seperated by
                  whitespace
  -d, --dest      Required. Destination directory to copy to
  -f, --format    Filetypes to be copied, seperated by ';'
  -m, --move      (Default: False) The files are to be moved
  --help          Display this help screen.
```