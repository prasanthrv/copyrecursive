﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;

namespace CopyRecursive
{
    class RecursiveRunner
    {
        // Formats specified by user
        public List<string> FileFormats { get; }
        // directories to copy files from
        public List<string> SourceDirs { get;  }
        // destination directory
        public string DestDir { get; }
        // copy or move files ?
        public bool ToMove { get; }

        public RecursiveRunner(string[] srcDirs, string destDir, List<string> flFormats = null, bool toMove = false )
        {
            SourceDirs = new List<string>();
            SourceDirs.AddRange(srcDirs);

            DestDir = destDir;
            ToMove = toMove;

            FileFormats = new List<string>(flFormats);
        }

        /// <summary>
        /// Get files recursively from a directory
        /// </summary>
        /// <param name="dir">Directory to scrape</param>
        /// <param name="exts">Allowed extensions</param>
        /// <returns>List of Files</returns>
        private List<string> GetFilesWithExtension(string dir, List<string> exts)
        {
            var dirs = new List<string>();
            dirs.Add(dir);
            return GetFilesWithExtension(dirs, exts);
        }

        // same as above, with multiple diretories
        private List<string> GetFilesWithExtension (List<string> dirs, List<string> exts)
        {
                List<string> srcFiles = new List<string>();
                // enumerate files to copy
                foreach (var srcPath in dirs)
                {
                    var allFiles = Directory.EnumerateFiles(srcPath, "*.*", SearchOption.AllDirectories);
                    // for debugging
                    StringBuilder filesText = new StringBuilder();

                    // if extensions are specified
                    if (exts?.Count != 0)
                    {
                        foreach (var file in 
                            from fl in allFiles
                            from ext in exts
                            where ext == Path.GetExtension(fl).Substring(1)
                            select fl)
                        {
                            srcFiles.Add(file);
                            filesText.AppendLine(file);
                        }
                    }
                    else
                        foreach (var file in allFiles)
                        {
                            srcFiles.Add(file);
                            filesText.AppendLine(file);
                        }

                    //Console.WriteLine(filesText);
                }
            return srcFiles;
        }

        /// <summary>
        /// Get the next usuable file name
        /// eg, if File exists, then File_1,File_2, File_3 etc
        /// are considered candidates
        /// </summary>
        /// <param name="file">File to get the next name of</param>
        /// <returns>Next available name in sequence</returns>
        private string GetNextName(string file)
        {
            string fileName = Path.GetFileNameWithoutExtension(file);
            string dir = Path.GetDirectoryName(file);
            string ext = Path.GetExtension(file);
            string sep = Path.DirectorySeparatorChar.ToString();

            string newFile = file;
            string newFileName = fileName;

            var counter = 1;
            while(File.Exists(newFile))
            {

                newFileName = fileName + $"_{counter++}";
                newFile = dir + sep + newFileName + ext;
            }

            return newFileName;
        }

        /// <summary>
        /// Return a list of files similar to the given file
        /// using pattern filename_[1,2..].extension
        /// by searching through the existing file list
        /// </summary>
        /// <param name="file">File to compare to</param>
        /// <param name="fileList">A string containing the list of files seperated by ';' </param>
        /// <returns>List of files</returns>
        private List<string> GetRelatedFiles(string file, string fileList)
        {
            string fileName = Path.GetFileNameWithoutExtension(file);
            Regex pattern = new Regex(";" + fileName + @"_[\d]+(\.(.*?))*(?=;)");
            var matches = pattern.Matches(fileList);
            List<string> files = new List<string>();

            foreach (Match match in matches)
                files.Add(match.Value.Substring(1));
            return files;
        }

        /// <summary>
        /// Compares two files for size
        /// </summary>
        /// <param name="file1">First file</param>
        /// <param name="file2">Second file</param>
        /// <returns>True on same size</returns>
        private bool IsSameSize(string file1, string file2)
        {
            var fl1 = new FileInfo(file1);
            var fl2 = new FileInfo(file2);

            return fl1.Length == fl2.Length;
        }

        /// <summary>
        /// Run the main loop for copying files
        /// </summary>
        public void run()
        {
            try
            {
                // create destination directory if not exists
                if (!Directory.Exists(DestDir))
                    Directory.CreateDirectory(DestDir);

                // get files to copy
                var srcFiles = GetFilesWithExtension(SourceDirs, FileFormats);

                // build list of files already existing
                StringBuilder existingDestFiles = new StringBuilder();
                existingDestFiles.Append(";");
                foreach (var file in GetFilesWithExtension(DestDir, FileFormats))
                    existingDestFiles.Append(Path.GetFileName(file) + ";");

                var filesCopied = 0;

                foreach(var srcFile in srcFiles)
                {

                
                    string fileName = Path.GetFileNameWithoutExtension(srcFile);
                    string ext = Path.GetExtension(srcFile);
                    string sep = Path.DirectorySeparatorChar.ToString();

                    string destFile = DestDir + sep + fileName + ext;
                    string newName = fileName;

                    // if same file and is duplicate, skip
                    if (File.Exists(destFile) && IsSameSize(srcFile, destFile))
                        continue;

                    // if same name is present, get a new name
                    if (File.Exists(destFile))
                        newName = GetNextName(destFile);

                    // get list of related files, file_1, file_2 .... 
                    var relatedFiles = GetRelatedFiles(destFile, existingDestFiles.ToString());

                    string newFile = DestDir + sep + newName + ext;
                    bool isDuplicate = false;
                    // see if any of the related file is duplicate of current file
                    // by checking size
                    foreach (var relFileName in relatedFiles)
                    {
                        var relFile = DestDir + sep + relFileName;
                        if (IsSameSize(srcFile, relFile))
                        {
                            isDuplicate = true;
                            break;
                        }
                    }

                    // copy or move if not a duplicate
                    if (!isDuplicate)
                    {
                        if (ToMove)
                            File.Move(srcFile, newFile);
                        else
                            File.Copy(srcFile, newFile);
                        existingDestFiles.Append(Path.GetFileName(newFile) + ";");
                        filesCopied++;
                    }

                }

                Console.WriteLine();
                Console.WriteLine($"Copied {filesCopied} files");
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}
