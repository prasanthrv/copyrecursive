﻿using System.Collections.Generic;

using CommandLine;
using CommandLine.Text;

namespace CopyRecursive
{
    /// <summary>
    /// This class represents the various options that can be passed into the 
    /// program using CommandLine.
    /// </summary>
    class Options
    {
        [OptionArray('s', "src", Required = true, HelpText = "Source directories to read from seperated by whitespace")]
        public string[] SourceDirs { get; set; }

        [Option('d', "dest", Required = true, HelpText = "Destination directory to copy to")]
        public string DestinationDir { get; set; }

        [OptionList('f',"format", HelpText = "Filetypes to be copied, seperated by ';'",Separator =';')]
        public List<string> Formats { get; set; }

        [Option('m', "move", DefaultValue = false, HelpText = "The files are to be moved")]
        public bool IsToBeMoved { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        /// <summary>
        /// Generate help screen
        /// </summary>
        /// <returns>Help string</returns>
        [HelpOption]
        public string GetUsage()
        {
            var help = new HelpText
            {
                Heading = new HeadingInfo("Copy Recursive", "0.1"),
                Copyright = new CopyrightInfo("Prasanth Ravi", 2016),
                AdditionalNewLineAfterOption = true,
                AddDashesToOption = true
            };
            help.AddPreOptionsLine("MIT Lisence");
            help.AddPreOptionsLine("Usage: CopyRecursive -s <src_dir1> <src_dir2> -d <dest_dir> -f \"txt;jpg\" -m0");
            help.AddOptions(this);
            return help;
        }

    }
}
