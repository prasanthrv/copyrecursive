﻿using System;
using System.Collections.Generic;

namespace CopyRecursive
{
    class Program
    {
        static void Main(string[] args)
        {

            var options = new Options();
            if (CommandLine.Parser.Default.ParseArguments(args,options))
            {

                //DebugParsedInfo(options);

                // get file formats
                List<string> fileFormats = new List<string>();
                if (options.Formats != null)
                    fileFormats.AddRange(options.Formats);

                // run main copier
                RecursiveRunner runner = new RecursiveRunner(options.SourceDirs, options.DestinationDir,
                   fileFormats, options.IsToBeMoved);
                runner.run();

            }
        }

        // print out input params
        static void DebugParsedInfo(Options options)
        {
                Console.WriteLine("Source paths:");
                if (options.SourceDirs != null)
                foreach (var srcPath in options.SourceDirs)
                    Console.WriteLine(srcPath);


                Console.WriteLine("Dest path:");
                if (options.DestinationDir != null)
                    Console.WriteLine(options.DestinationDir);

                List<string> fileFormats = new List<string>();
                Console.WriteLine("File formats:");
                if (options.Formats != null)
                    fileFormats.AddRange(options.Formats);
                foreach (var format in fileFormats)
                    Console.WriteLine(format);


        }
    }
}
